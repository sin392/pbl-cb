class Person:
    def __init__(self, userid, name, age, sex):
        self.userid = userid
        self.name = name
        self.age = age
        self.sex = sex

    def data(self):
        return (self.userid, self.name, self.age, self.sex)

    def save(self):
        # データベースにデータを保存
        pass

if __name__ == "__main__":
    
    chara4 = Person('userid', 'Kazuma', 16, 'male') # インスタンス作成時にコンストラクタが呼び出される
    print(*chara4.data())