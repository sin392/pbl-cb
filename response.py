import config as c
from read_message import read_message
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage, TemplateSendMessage, ConfirmTemplate, 
    PostbackTemplateAction, MessageTemplateAction
)

def TSM(*args):
    n = len(args)
    send_text = [TextSendMessage(text=args[i]) for i in range(n)]
    return send_text

def get_response(event):
    emt = event.message.text

    if emt == "問診" and c.flag == 0:
        messages = TSM("問診を開始します","あなたのお名前を教えてください")
        c.flag = 1
    elif emt == "終了" and c.flag != 0:
        messages = TSM("問診を終了します")
        c.flag = 0
    elif c.flag == 1:
        c.data["name"] = emt
        # messages = TSM("あなたのお名前は {} ですか？".format(c.data["name"]))
        # messageTemplate = TemplateSendMessage(
        #     alt_text='Confirm template',
        #     template=ConfirmTemplate(
        #         text='Are you sure?',
        #         actions=[
        #             PostbackTemplateAction(
        #                 label='postback',
        #                 text='postback text',
        #                 data='action=buy&itemid=1'
        #             ),
        #             MessageTemplateAction(
        #                 label='message',
        #                 text='message text'
        #             )
        #         ]
        #     )
        # )

        # messages = messageTemplate
        # messages.append(messageTemplate)
        messages = TSM(*read_message(c.data,c.message_df))
    else:
        # messages = TSM(emt)
        messages = TSM(*read_message(c.data,c.message_df))
    return messages