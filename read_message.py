import pandas as pd
import numpy as np
from jinja2 import Template
import config as c

def func(df):
    series = df[df["flag"] == c.flag]
    c.flag_pre = c.flag
    c.flag = series["next"].values[0]
    return series

def make_message(data,series):
    column_list = ["message{}".format(i) for i in range(1,4)]
    message = series[column_list].values[0]
    message = message[[type(x) is str for x in message]]
    message = [Template(x) for x in message]
    message = [message[i].render(data) for i in range(len(message))]
    return message

def read_term(data,series):
    term = series["term"].values[0]
    if type(term) is str:
        term = Template(term)
        term = term.render(data)
        return term
    else:
        return None

def exec_term(term):
    if type(term) is str:
        term = Template(term)
        term = term.render(data)
        exec(term)
    else:
        print("No term")

def read_message(data,df):
    series = func(df)
    disp_text = make_message(data,series)
    print(c.flag)
    # term = read_term(data,series)
    # exec_term(term)
    return disp_text

if __name__ == "__main__":
    c.flag = 1
    data = {"emt":"問診","name":"Shin"}
    df = pd.read_csv("message.csv")
    for i in range(len(df)):
        # series, flag = func(df,flag)
        # disp_text = make_message(series)
        # term = read_term(series)
        # print(term)
        # exec_term(term)
        print(c.flag)
        disp_text = read_message(data,df)
        print(disp_text)