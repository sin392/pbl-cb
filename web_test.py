# from jinja2 import Template
 
# tpl_text = '僕の名前は{{ name }}です！！{{ lang }}が好きです！！'
# template = Template(tpl_text)
 
# data = {'name': 'Kuro', 'lang': 'Python'}
# disp_text = template.render(data) # 辞書で指定する
# print(disp_text) # 僕の名前はKuroです！！Pythonが好きです！！

from jinja2 import Template, Environment, FileSystemLoader
 
env = Environment(loader=FileSystemLoader('.'))
template = env.get_template('sample.tpl')
 
data = {'name': 'Kuro', 'lang': 'Python'}
disp_text = template.render(data) # 辞書で指定する
print(disp_text)